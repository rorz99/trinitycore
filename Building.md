
# Building the container image

Building the container image is optional as you can simply pull the latest
`nicolaw/trinitycore` image directly from [DockerHub](https://hub.docker.com/r/nicolaw/trinitycore/).

Alternatively you can compile TrinityCore inside a container to build the
container image directly with `docker` or using the convenience Makefile wrapper
using the `build` target:

    # Building manually with Docker
    docker build -f Dockerfie .
    
    # Convenience Makefile wrapper
    make build

Two different image types can be created by specifying the `VARIANT` variable
with the `build` target. If you only need to TrinityCore binaries (`authserver`,
`worldserver` and the map data extraction tools), then the `slim` variant should
be sufficient. Most people will probably want to use the `sql` (the default if
not explcitly set otherwise) variant as it also includes all the SQL files
needed to bootstrap all the databases needed by the `authserver` and
`worldserver`.

    make build VARIANT=slim # minimal image size without SQL files
    make build VARIANT=sql  # includes all SQL files needed to populate the DB
    make build VARIANT=bots # alternate build that includes Trinity-Bots patch
    make build              # defaults to the same as VARIANT=sql

# Downloading SQL database

If you use the `sql` variant image (`nicolaw/trinitycore:3.3.5-sql`) then you
will already have everything you need to run your TrinityCore server, including
populating and your database from scratch.

    $ docker run --rm -it nicolaw/trinitycore:3.3.5-sql
    / # ls -lh /*sql* /src/*
    lrwxrwxrwx    1 root     root          47 Mar 12 12:40 /TDB_full_world_335.21021_2021_02_15.sql -> src/sql/TDB_full_world_335.21021_2021_02_15.sql
    lrwxrwxrwx    1 root     root           7 Mar 12 12:40 /sql -> src/sql
    
    /src/sql:
    total 255M   
    -rw-r--r--    1 root     root      255.2M Feb 15 12:37 TDB_full_world_335.21021_2021_02_15.sql
    drwxr-xr-x    3 root     root        4.0K Mar 11 23:50 base
    drwxr-xr-x    2 root     root        4.0K Mar 11 23:50 create
    drwxr-xr-x    5 root     root        4.0K Mar 11 23:50 custom
    drwxr-xr-x    9 root     root        4.0K Mar 11 23:50 old
    drwxr-xr-x    5 root     root        4.0K Mar 11 23:50 updates

The `sql` variant image is considerably larger than the `slim` image. If you
use the smaller image and still need to download the SQL data. You can either
use the `gettdb` and `getsql` helper scripts or run the `tdb` and `sql`
Makefile targets:

    # Download TDB full world database
    ./gettdb
    make tdb

    # Download base SQL schemas and updates
    ./getsql
    make sql
