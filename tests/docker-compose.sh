#!/bin/sh
# MIT License
# Copyright (c) 2017-2022 Nicola Worthington <nicolaw@tfb.net>
# https://gitlab.com/nicolaw/trinitycore

# TODO: Test that we can then talk to the worldserver SOAP API okay.

set -e

healthyContainers () {
  docker-compose ps \
    | grep -c -E '^(authserver|mysql|worldserver)[[:space:]].*[[:space:]]Up \((healthy|health: starting)\)[[:space:]]'
}

containersStarted () {
  docker logs authserver 2>&1 | grep -E '^INFO  Added realm ".*" at .*\.'
  docker logs worldserver 2>&1 | grep -E '^INFO  TrinityCore rev. .* \(worldserver-daemon\) ready\.\.\.'
}

cleanUp () {
  rm -f -- mysql.log worldserver.log authserver.log

  for action in stop kill rm
  do
    docker-compose -f ../docker-compose.yaml "$action" || true
    sleep 5
  done
}

main () {
  if [ -e tests/docker-compose.sh ] && [ -e docker-compose.yaml ]
  then
    cd ./tests/
  fi

  cleanUp

  startTime="$(date +%s)"
  deadlineTime=$((startTime + 300))

  docker-compose -f ../docker-compose.yaml up --detach

  while [ "$(date +%s)" -lt "$deadlineTime" ]
  do
    sleep 10

    docker-compose ps
    docker logs mysql       2>&1 | tee mysql.log       | tail | sed -e 's/^/mysql\t/'
    docker logs worldserver 2>&1 | tee worldserver.log | tail | sed -e 's/^/worlserver\t/'
    docker logs authserver  2>&1 | tee authserver.log  | tail | sed -e 's/^/authserver\t/'
    echo ""

    if [ "$(healthyContainers)" -ne 3 ] || containersStarted
    then
      break
    fi
  done

  rc=0
  [ "$(healthyContainers)" -eq 3 ] || rc=1
  containersStarted || rc=$?

  docker-compose -f ../docker-compose.yaml stop

  exit $rc
}

main "$@"

