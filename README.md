# TrinityCore

This is a slim container image for TrinityCore 3.3.5.

* [Getting started guide](https://gitlab.com/nicolaw/trinitycore/-/blob/master/GettingStarted.md) - simple tutorial to running your own World of Warcraft WotLK TrinityCore server
* [Building the container image](https://gitlab.com/nicolaw/trinitycore/-/blob/master/Building.md) - guide to building the container image yourself
* [AWS CloudFormation template](https://gitlab.com/nicolaw/trinitycore/-/tree/master/aws) - fully automated deployment into Amazon Web Services
* [Docker Hub container image](https://hub.docker.com/r/nicolaw/trinitycore/)
* [Dockerfile](https://gitlab.com/nicolaw/trinitycore/-/blob/master/Dockerfile)
* [docker-compose.yaml](https://gitlab.com/nicolaw/trinitycore/-/blob/master/docker-compose.yaml)

## Synopsis

    # Describe information about the container image.
    $ docker pull nicolaw/trinitycore
    $ docker inspect nicolaw/trinitycore | jq -r '.[0].Config.Labels'
    $ docker run --rm -it nicolaw/trinitycore sh -c "ls -lh /opt/trinitycore/* /etc/*server.conf* /usr/local/bin"
    
    # Extract map data using helper script.
    $ docker run --rm -it -v $PWD/World_of_Warcraft:/wow -v $PWD/mapdata:/mapdata -w /mapdata -it nicolaw/trinitycore genmapdata /wow /mapdata
    
    # Extract map data manually.
    $ docker run --rm -it -v $PWD/World_of_Warcraft:/wow -v $PWD/mapdata:/mapdata -w /mapdata -it nicolaw/trinitycore mapextractor -i /wow -o /mapdata -e 7 -f 0
    $ docker run --rm -it -v $PWD/World_of_Warcraft:/wow -v $PWD/mapdata:/mapdata -w /mapdata -it nicolaw/trinitycore vmap4extractor -l -d /wow/Data
    $ docker run --rm -it -v $PWD/World_of_Warcraft:/wow -v $PWD/mapdata:/mapdata -w /mapdata -it nicolaw/trinitycore vmap4assembler /mapdata/Buildings /mapdata/vmaps
    $ docker run --rm -it -v $PWD/World_of_Warcraft:/wow -v $PWD/mapdata:/mapdata -w /mapdata -it nicolaw/trinitycore mmaps_generator
    
    # Run authserver and worldserver in the background.
    $ docker run --name authserver -d --restart unless-stopped --rm -p 3724:3724 -v $PWD/authserver.conf:/etc/authserver.conf nicolaw/trinitycore authserver
    $ docker run --name worldserver -d --restart unless-stopped --rm -it -p 8085:8085 -p 3443:3443 -p 7878:7878 -v $PWD/worldserver.conf:/etc/worldserver.conf -v $PWD/mapdata:/mapdata nicolaw/trinitycore worldserver

## Running

### Authentication server

    docker run --rm -p 3724:3724 -v $PWD/authserver.conf:/etc/authserver.conf -d nicolaw/trinitycore authserver

### Height map data generation for world server

The following command assumes that your World of Warcraft 3.3.5 (WotLK) game client is located in the `$PWD/World_of_Warcraft` directory
on your local machine.

    mkdir ./mapdata/
    chmod a+rwx ./mapdata/
    docker run --rm -it -v $PWD/World_of_Warcraft:/wow -v $PWD/mapdata:/mapdata -w /mapdata -it nicolaw/trinitycore genmapdata /wow /mapdata

This process will take several hours to produce approximately 4 GB of data inside the `mapdata` directory.

### World server

    docker run --rm -p 8085:8085 -p 3443:3443 -p 7878:7878 -v $PWD/worldserver.conf:/etc/worldserver.conf -v $PWD/mapdata:/mapdata -d -it nicolaw/trinitycore worldserver

### Docker compose

A convenience [docker-compose.yaml](https://gitlab.com/nicolaw/trinitycore/-/blob/master/docker-compose.yaml)
is available at <https://gitlab.com/nicolaw/trinitycore/-/blob/master/docker-compose.yaml> for easy operation of the
database, world server and auth server using `docker-compose`:

    docker-compose up

Refer to the Docker Compose documentation at <https://docs.docker.com/compose> for
more information.

## Configuration files and customisation

Default configuration files are in `/etc/authserver.conf` and `/etc/worldserver.conf` inside the container image.
These may be modified outside of the container and mounted as volumes into the container to customise the
operation of the authentication and world servers:

    docker run --rm nicolaw/trinitycore cat /etc/authserver.conf > authserver.conf
    docker run --rm nicolaw/trinitycore cat /etc/worldserver.conf > worldserver.conf

See <https://trinitycore.atlassian.net/wiki/spaces/tc/overview> for more information.

## Account creation

Assuming that you started the world server container with an interactive TTY (the `-it` argument to `docker run`),
you may attach to the container to execute to run interactive commands as documented by
<https://trinitycore.atlassian.net/wiki/spaces/tc/pages/2130065/gm+commands>.

    docker ps
    docker attach <container-ID>

Pressing `Control-P` then `Control-Q` will disconnect you from the container console.

Example:

    $ docker ps
    CONTAINER ID   IMAGE                        COMMAND                  CREATED        STATUS        PORTS     NAMES
    b897d9c319e5   nicolaw/trinitycore:latest   "worldserver -c /etc…"   23 hours ago   Up 23 hours             worldserver
    
    $ docker attach b897d9c319e5
    TC> .account create janedoe letmein jane.doe@example.com
    TC> INFO  CLI command under processing...
    Account created: janedoe
    TC> .account delete janedoe
    TC> INFO  CLI command under processing...
    Account deleted: JANEDOE
    TC> read escape sequence

### tcpasswd

The `tcpasswd` helper command from `docker.io/nicolaw/tcpasswd:latest` is
included in this container image. This provides a convenient means of
generating username and passwords that may be inserted into the TrinityCore
`auth` database.

    docker run --rm nicolaw/trinitycore tcpasswd --help
    docker run --rm nicolaw/trinitycore tcpasswd --username janedoe --password letmein

The SQL output from this command may be sent directly into MySQL:

    docker run --rm nicolaw/trinitycore tcpasswd \
      --username janedoe --password letmein --output sql \
      | mysql -P 3306 -u trinity -D auth

Refer to <https://gitlab.com/nicolaw/tcpasswd/> and
<https://hub.docker.com/r/nicolaw/tcpasswd> for more information.
