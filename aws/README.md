# Amazon Web Services

## CloudFormation template

The [trinitycore-cfn.yaml](trinitycore-cfn.yaml) CloudFormation template
provides a quick and easy means to deploy a fully functional world server
along side a supporting database and authentiction server for personal use.

Beginners will liklely prefer to use the AWS CloudFormation web console UI
at https://console.aws.amazon.com/cloudformation.

![AWS CloudFormation Designer overview diagram of trinitycore-cfn.yaml](CloudFormationDesigner.png)

## World server height map data

The CloudFormation template provides three mechanisms to supply the height map
data to the world server.

1. The URL of a Tar archive containing the height map data may be passed to the
   template at deployment time. This will be downloaded and extracted onto the
   world server automatically. Refer to
   https://gitlab.com/nicolaw/trinitycore/-/packages for an acompany list of
   pre-generated height map data archives.

2. Pre-generated hight map data may be manually copied into the S3 bucket
   created by the CloudFormation template. Objects should be located under the
   `mapdata/` object namespace. See the CloudFormation stack output values
   for the exact path to upload to.

3. The world server EC2 instance can generate the height map data itself. This
   requires that the World of Warcraft game client files be copied into the S3
   bucket, under the `World_of_Warcraft/` object namespace. See the
   CloudFormation stack output values for the exact path to upload to and an
   example AWS CLI command. This may take a few hours to generate depending on
   the EC2 instance type selected for the world server. Low cost burstable T
   series instance types are not well suited to this method unless they have
   the unlimited CPU credits feature enabled.
